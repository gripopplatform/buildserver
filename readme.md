# Schulinck PHP development environment setup

Schulinck will become a dockerized environment, building and deployment of these docker images is done via Jenkins.
Storage of the docker images is handled in nexus. During the software builds the codequality can be scanned via Sonar.

jenkins is available at: http://172.25.5.21:8080
nexus is available at: http://172.25.5.21:8081
sonar is available at: http://172.25.5.21:9000

# setting up jenkins 

- clone this repository ( git@bitbucket.org:gripopplatform/buildserver.git )
- goto the jenkins folder `cd jenkins`
- copy your ssh keys (~/.ssh/id_rsa and ~/.ssh/id_rsa.pub) to the credentials folder, this key will be used by jenkins jobs to authorize actions on remote servers.
- run `docker-compose up --build`

A default installation will now build and once finished be available at http://localhost:8080

# updating jenkins & plugins

From the plugin manager, update all available plugins. Once done, run the following

```
export JENKINS_HOST=localhost:8080
export JENKINS_USER=admin
export JENKINS_PASS=admin

curl -sSL "http://$JENKINS_USER:$JENKINS_PASS@$JENKINS_HOST/pluginManager/api/xml?depth=1&xpath=/*/*/shortName|/*/*/version&wrapper=plugins" | perl -pe 's/.*?<shortName>([\w-]+).*?<version>([^<]+)()(<\/\w+>)+/\1 \2\n/g'|sed 's/ /:/'
```

This will output a list of installed plugins and versions, copy-paste the list into plugins.txt and commit the file. Next time the docker image is built, the updated versions will be installed.

