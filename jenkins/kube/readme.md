# Usage
This config file here is a dummy file, it should be replaced locally with the kube configuration file for the kubernetes cluster that should be managed by this jenkins instance. This file is usually located at `~/.kube/config`
After copying run `make init` to mount the config file into the correct location and initialize helm.