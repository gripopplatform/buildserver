# Private docker registry
This is the private registry running on 172.25.5.21:5000, to use this create or modify /etc/docker/daemon.json to contain { "insecure-registries":["172.25.5.21:5000"] } and restart docker. You can now tag the image with the registry name prefixed to it like so: docker image tag base-image:latest 172.25.5.21:5000/php72-apache:latest and push it to the repository: docker push 172.25.5.21:5000/php72-apache:latest

# Running this contraption
is easy, just execute: `docker-compose up -d` from this files directory.